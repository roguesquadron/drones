package com.tomtom.rogue.stalker.translator;

/**
 * Created by cichy on 7/22/14.
 */
public interface StalkerVectorTranslator {
    /**
     * Translates current position against runner into
     * drone commands (to be defined returned type)
     * @param currentVector
     */
    AdjustmentOrder translate(double[] currentVector);

    /**
     * Initial value of the vector
     * @param initialVector
     */
    void init(double[] initialVector);

    /**
     * Initial value of the vector with predefined tolerance
     * @param initialVector
     * @param tolerance
     */
    void init(Vector initialVector, int tolerance);

}
