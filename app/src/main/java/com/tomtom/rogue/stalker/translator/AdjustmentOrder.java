package com.tomtom.rogue.stalker.translator;

/**
 * Created by cichy on 7/24/14.
 */
public class AdjustmentOrder {
    DirectionAdjustmentOrder direction;
    SpeedAdjustmentOrder speed;
    HeightAdjustmentOrder height;

   public DirectionAdjustmentOrder getDirection() {
        return direction;
    }

    public void setDirection(DirectionAdjustmentOrder direction) {
        this.direction = direction;
    }

    public SpeedAdjustmentOrder getSpeed() {
        return speed;
    }

    public void setSpeed(SpeedAdjustmentOrder speed) {
        this.speed = speed;
    }

    public HeightAdjustmentOrder getHeight() {
        return height;
    }

    public void setHeight(HeightAdjustmentOrder height) {
        this.height = height;
    }

    /**
     * Created by cichy on 7/24/14.
     */
    public static enum DirectionAdjustmentOrder {
        GO_LEFT,GO_RIGHT,STEADY
    }

    /**
     * Created by cichy on 7/24/14.
     */
    public static enum HeightAdjustmentOrder {
        UP, DOWN, STEADY, LAND
    }

    /**
     * Created by cichy on 7/24/14.
     */
    public static enum SpeedAdjustmentOrder {
        SPEED_UP,SLOW_DOWN,STOP, KEEP_PACE
    }
}
