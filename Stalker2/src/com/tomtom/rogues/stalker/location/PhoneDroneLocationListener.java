package com.tomtom.rogues.stalker.location;

import com.parrot.freeflight.drone.NavData;
import com.parrot.freeflight.ui.hud.Text;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;


public class PhoneDroneLocationListener implements LocationListener {
  private  Text phoneLocationText;
   private Text droneLocationText;
   private int updateNumber=0;
   private DroneLocation droneLocation;

    public PhoneDroneLocationListener(Text phoneLocationText, Text droneLocationText) {
        super();
        this.phoneLocationText = phoneLocationText;
        this.droneLocationText = droneLocationText;
        droneLocation=new DroneLocation();
    }

    public void updateNavGpsData(NavData navData){
        droneLocation.updateNavData(navData);
    }
    @Override
    public void onLocationChanged(Location location) {
        phoneLocationText.setVisible(false);
        if(location.hasAccuracy()){
            updateNumber++;
//            if(location.getAccuracy()>4.0){
                phoneLocationText.setVisible(true);
       phoneLocationText.setText("Phone, lat: "+location.getLatitude()+" lon: "+location.getLongitude()+" accurancy: "+location.getAccuracy()+" update: "+updateNumber);
//            
       
        
        }
//       if(droneLocation.getIsDataAvailable()){
//           droneLocationText.setVisible(true);
//           phoneLocationText.setText("Phone, lat: "+droneLocation.getLatitude()+" lon: "+droneLocation.getLongitude()+" accurancy: "+droneLocation.getAccuracy()+" update: "+updateNumber);
//       }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        phoneLocationText.setVisible(true);
   droneLocationText.setVisible(true);

    }

    @Override
    public void onProviderDisabled(String provider) {
        phoneLocationText.setVisible(false);
        droneLocationText.setVisible(false);

    }

}
