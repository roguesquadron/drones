package com.tomtom.rogues.stalker.location;

import com.parrot.freeflight.drone.NavData;


public class DroneLocation {
    private double latitude;
    private double longitude;
    private double accurancy0;
    private double accurancy1;
    private double accurancy2;
    
    private float degree;
    private  float degreeMagnetic; 
    private boolean isGpsPlugged;
    private boolean isDataAvailable;
    public double gpsTime;
    
    
    
    public void updateNavData(NavData navData){
        latitude=navData.lat;
        longitude=navData.lon;
        accurancy0=navData.accurancy0;
        accurancy1=navData.accurancy1;
        accurancy2=navData.accurancy2;
        degree=navData.degree;
        degreeMagnetic=navData.degreeMagnetic; 
        isGpsPlugged=navData.isGpsPlugged>0 ? true : false;
        isDataAvailable=navData.isDataAvailable>0 ? true : false;
        gpsTime=navData.gpsTime;
    }
    
    public double getLatitude() {
        return latitude;
    }
    
    public double getLongitude() {
        return longitude;
    }
    
    public double getAccurancy0() {
        return accurancy0;
    }
    
    public double getAccurancy1() {
        return accurancy1;
    }
    
    public double getAccurancy2() {
        return accurancy2;
    }
    
    public float getDegree() {
        return degree;
    }
    
    public float getDegreeMagnetic() {
        return degreeMagnetic;
    }

    public boolean getIsGpsPlugged() {
        return isGpsPlugged;
    }

    
    public boolean getIsDataAvailable() {
        return isDataAvailable;
    }

    public double getAccuracy() {
        return accurancy0+accurancy1+accurancy2;
    }
    
}
