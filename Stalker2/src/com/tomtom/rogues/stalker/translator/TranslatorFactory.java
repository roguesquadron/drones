package com.tomtom.rogues.stalker.translator;

/**
 * Created by cichy on 7/22/14.
 */
public class TranslatorFactory {

    private static TranslatorFactory ourInstance = new TranslatorFactory();

    public static TranslatorFactory getInstance() {
        return ourInstance;
    }

    private TranslatorFactory() {
    }

    public StalkerVectorTranslator getTranslator(TranslatorStrategy strategy) {
        switch (strategy)
        {
            case SIMPLE:
                return new SimpleTranslator();
            case ADAPTIVE:
                return null;
            default:
                return null;
        }


    }

}
