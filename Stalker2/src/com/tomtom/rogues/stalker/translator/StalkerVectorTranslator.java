package com.tomtom.rogues.stalker.translator;

import jp.nyatla.nyartoolkit.core.param.NyARFrustum;
import jp.nyatla.nyartoolkit.core.types.matrix.NyARDoubleMatrix44;

/**
 * Created by cichy on 7/22/14.
 */
public interface StalkerVectorTranslator {
    /**
     * Translates current position against runner into
     * drone commands (to be defined returned type)
     * @param currentMatrix
     */
    AdjustmentOrder translate(NyARDoubleMatrix44 currentMatrix);

    /**
     * Initial value of the vector
     * @param initialMatrix
     * @param frustum
     * @param screenWidth
     * @param screenHeight
     */
    void init(NyARDoubleMatrix44 initialMatrix, NyARFrustum frustum, int screenWidth, int screenHeight);

    /**
     * Initial value of the vector with predefined tolerance
     * @param initialMatrix
     * @param tolerance
     */
    void init(NyARDoubleMatrix44 initialMatrix, NyARFrustum frustum, int screenWidth, int screenHeight, int tolerance);

}
