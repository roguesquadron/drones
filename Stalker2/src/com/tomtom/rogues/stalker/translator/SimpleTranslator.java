package com.tomtom.rogues.stalker.translator;

import android.util.Log;

import jp.nyatla.nyartoolkit.core.param.NyARFrustum;
import jp.nyatla.nyartoolkit.core.types.NyARDoublePoint2d;
import jp.nyatla.nyartoolkit.core.types.matrix.NyARDoubleMatrix44;

/**
 * Created by cichy on 9/30/14.
 */
public class SimpleTranslator implements StalkerVectorTranslator {
    private static int DEFAULT_XY_TOLERANCE = 10;
    private static int  DEFAULT_Z_TOLERANCE = 5;
    private float tolerance = DEFAULT_XY_TOLERANCE;
    private double x, y, z;
    private int screenWidth, screenHeigth;
    private double xAbsoluteTolerance, yAbsoluteTolerance, zAbsoluteTolerance;
    private NyARFrustum frustum;

    @Override
    public AdjustmentOrder translate(NyARDoubleMatrix44 currentMatrix) {
        AdjustmentOrder order = new AdjustmentOrder();
        NyARDoublePoint2d point2d = new NyARDoublePoint2d();
        frustum.project(currentMatrix.m03,currentMatrix.m13,currentMatrix.m23,point2d );
        calculateXPosition(point2d, order);
        calculateYPosition(point2d, order);
        calculateSpeed(currentMatrix, order);
        Log.i("Calculated orders", "Horizontal: " + order.getDirection() + "Vertical: " + order.getHeight() + "Speed: " + order.getSpeed());
        return order;
    }

    @Override
    public void init(NyARDoubleMatrix44 initialMatrix, NyARFrustum frustum, int screenWidth, int screenHeight) {
        NyARDoublePoint2d point2d = new NyARDoublePoint2d();
        frustum.project(initialMatrix.m03,initialMatrix.m13,initialMatrix.m23,point2d );
        this.frustum = frustum;
        x=point2d.x;
        y=point2d.y;
        z=initialMatrix.m23;
        this.screenHeigth = screenHeight;
        this.screenWidth = screenWidth;
        xAbsoluteTolerance = (screenWidth*tolerance)/100;
        yAbsoluteTolerance = (screenHeigth*tolerance)/100;
        zAbsoluteTolerance = (z*DEFAULT_Z_TOLERANCE)/100;

        Log.i("Calculated orders", "inittt "+x+" "+y+" "+z);
        Log.i("Calculated orders","X tolerance: " + xAbsoluteTolerance + "Y tolerance: " + yAbsoluteTolerance + "Z tolerance: " + zAbsoluteTolerance);

    }

    @Override
    public void init(NyARDoubleMatrix44 initialMatrix,NyARFrustum frustum, int screenWidth, int screenHeight, int tolerance) {
        this.tolerance = tolerance;
        init(initialMatrix,frustum,screenWidth,screenHeight );
        Log.i("Calculated orders: init", String.valueOf(initialMatrix.m03));
    }

    void calculateXPosition(NyARDoublePoint2d point2d, AdjustmentOrder order) {
        double xdiff = point2d.x - x;
        Log.i("Horizontal difference: X ", String.valueOf(xdiff));
        if (Math.abs(xdiff) < xAbsoluteTolerance) {
            order.setDirection(AdjustmentOrder.DirectionAdjustmentOrder.STEADY);
            return;
        }
        if (xdiff > 0) {
            order.setDirection(AdjustmentOrder.DirectionAdjustmentOrder.GO_RIGHT);
            return;
        }
        if (xdiff <= 0) {
            order.setDirection(AdjustmentOrder.DirectionAdjustmentOrder.GO_LEFT);

        }
    }

    void calculateYPosition(NyARDoublePoint2d point2d, AdjustmentOrder order) {
        //y axis starts top left corner, increasing downwards with positive numbers
        double ydiff = point2d.y - y;
        Log.i("Vertical difference: Y ", String.valueOf(ydiff));
        if (Math.abs(ydiff) < (double) yAbsoluteTolerance) {
            order.setHeight(AdjustmentOrder.HeightAdjustmentOrder.STEADY);
            return;
        }
        if (ydiff > 0) {
            order.setHeight(AdjustmentOrder.HeightAdjustmentOrder.DOWN);
            return;
        }
        if (ydiff <= 0) {
            order.setHeight(AdjustmentOrder.HeightAdjustmentOrder.UP);
        }


    }

    void calculateSpeed(NyARDoubleMatrix44 currentMatrix, AdjustmentOrder order) {
        double zdiff = currentMatrix.m23 - z;
        Log.i("Distance difference:", String.valueOf(zdiff));
     double  absoluteZdiff=Math.abs(zdiff);
        double distanceFactor=Math.abs(currentMatrix.m23/z);
        if (Math.abs(zdiff *100/ z) < (double) DEFAULT_Z_TOLERANCE) {
            order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.STOP);
            return;
        }
        if (zdiff > 0) {
            if(zdiff<500){
            order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.SPEED_UP1);
            }
            else if(zdiff<800){
                order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.SPEED_UP2);  
            }
            else if(zdiff<1100){
                order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.SPEED_UP3);  
            }
            else if(zdiff>=1100){
                order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.SPEED_UP4);  
            }
            else{
                order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.STOP); 
            }
            return;
        }
        if (zdiff <= 0) {
            if(absoluteZdiff>200){
                order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.SLOW_DOWN);
            }
            else{
                order.setSpeed(AdjustmentOrder.SpeedAdjustmentOrder.STOP);
            }

        }


    }


}