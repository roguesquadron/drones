package com.tomtom.rogues.stalker.translator;

/**
 * Created by cichy on 7/24/14.
 */
public class AdjustmentOrder {
    DirectionAdjustmentOrder direction;
    SpeedAdjustmentOrder speed;
    HeightAdjustmentOrder height;

   public DirectionAdjustmentOrder getDirection() {
        return direction;
    }

    public void setDirection(DirectionAdjustmentOrder direction) {
        this.direction = direction;
    }

    public SpeedAdjustmentOrder getSpeed() {
        return speed;
    }

    public void setSpeed(SpeedAdjustmentOrder speed) {
        this.speed = speed;
    }

    public HeightAdjustmentOrder getHeight() {
        return height;
    }

    public void setHeight(HeightAdjustmentOrder height) {
        this.height = height;
    }

    /**
     * Created by cichy on 7/24/14.
     */
    public static enum DirectionAdjustmentOrder {
        GO_LEFT,GO_RIGHT,STEADY
    }

    /**
     * Created by cichy on 7/24/14.
     */
    public static enum HeightAdjustmentOrder {
        UP, DOWN, STEADY, LAND
    }

    /**
     * Created by cichy on 7/24/14.
     */
    public static enum SpeedAdjustmentOrder {
        SPEED_UP1(-0.2F), SPEED_UP2(-0.3F), SPEED_UP3(-0.4F),SPEED_UP4(-0.5F),SLOW_DOWN(0.2F),STOP(0F);
       
        
        float speed=0F;
        SpeedAdjustmentOrder(float speed){
            this.speed=speed;
        }
        
       public float getSpeedFactor(){
          return speed;
       };
    }
    
    @Override
    public String toString() {    
    	return "AO[" +direction+","+height+","+speed+"]";
    }
}
