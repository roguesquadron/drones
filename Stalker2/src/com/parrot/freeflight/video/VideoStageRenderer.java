/*
 * VideoStageRenderer Created on: May 20, 2011 Author: Dmytro Baryskyy
 */

package com.parrot.freeflight.video;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import jp.androidgroup.nyartoolkit.utils.camera.CameraPreview;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLBox;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLDebugDump;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLFpsLabel;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLTextLabel;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLView;
import jp.nyatla.nyartoolkit.core.NyARException;
import jp.nyatla.nyartoolkit.core.param.INyARCameraDistortionFactor;
import jp.nyatla.nyartoolkit.core.param.NyARCameraDistortionFactorV2;
import jp.nyatla.nyartoolkit.core.param.NyARPerspectiveProjectionMatrix;
import jp.nyatla.nyartoolkit.core.raster.rgb.NyARRgbRaster;
import jp.nyatla.nyartoolkit.core.types.NyARBufferType;
import jp.nyatla.nyartoolkit.core.types.NyARIntSize;
import jp.nyatla.nyartoolkit.core.types.matrix.NyARDoubleMatrix44;
import jp.nyatla.nyartoolkit.markersystem.NyARMarkerSystem;
import jp.nyatla.nyartoolkit.markersystem.NyARMarkerSystemConfig;
import jp.nyatla.nyartoolkit.markersystem.NyARSensor;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Environment;
import android.util.Log;

import com.google.android.maps.MyLocationOverlay;
import com.parrot.freeflight.R;
import com.parrot.freeflight.activities.ControlDroneActivity;
import com.parrot.freeflight.service.DroneControlService;
import com.parrot.freeflight.ui.gl.GLBGVideoSprite;
import com.parrot.freeflight.ui.hud.Button;
import com.parrot.freeflight.ui.hud.Sprite;
import com.parrot.freeflight.ui.hud.Text;
import com.tomtom.rogues.stalker.location.PhoneDroneLocationListener;
import com.tomtom.rogues.stalker.translator.AdjustmentOrder;
import com.tomtom.rogues.stalker.translator.AdjustmentOrder.SpeedAdjustmentOrder;
import com.tomtom.rogues.stalker.translator.StalkerVectorTranslator;
import com.tomtom.rogues.stalker.translator.TranslatorFactory;
import com.tomtom.rogues.stalker.translator.TranslatorStrategy;

public class VideoStageRenderer implements Renderer {

    public static final int SCREEN_WIDTH = 640;
    public static final int SCREEN_HEIGHT = 360;
    private GLBGVideoSprite bgSprite;
    int frame = 0;
    int frameLost = 0;
    private ArrayList<Sprite> sprites;
    private Map<Integer, Sprite> idSpriteMap;

    private float fps;

    private int screenWidth;
    private int screenHeight;

    // **********************
    private float[] mVMatrix = new float[16];
    private float[] mProjMatrix = new float[16];

    private int program;
    private boolean isEmulated;

    private int _mid;
    AndGLTextLabel text;
    AndGLBox box;
    AndGLFpsLabel fpsN;
    AndGLDebugDump _debug = null;

    CameraPreview _camera_preview;
    AndGLView _glv;
    Camera.Size _cap_size;
    AssetManager assetMng;
    private NyARMarkerSystemConfig nyARMarkerSystemConfig;
    private NyARSensor nyarS;
    NyARMarkerSystem nyarM;
    ControlDroneActivity context;
    Text txtVector;
    float defaultTurnFactor = 0.2F;
    float defaultSpeed = 0.2F;
    float fullSpeed = 0.4F;
    private boolean isFlying;
    private boolean markerFound = false;
    PhoneDroneLocationListener phoneDroneLocationListener;

    public void setPhoneDroneLocationListener(PhoneDroneLocationListener phoneDroneLocationListener) {
        this.phoneDroneLocationListener = phoneDroneLocationListener;
    }

    private final String vertexShaderCode = "uniform mat4 uMVPMatrix;   \n" + "attribute vec4 vPosition; \n"
        + "attribute vec2 aTextureCoord;\n" + "varying vec2 vTextureCoord;\n" + "void main(){              \n"
        + "  gl_Position = uMVPMatrix * vPosition; \n" + "  vTextureCoord = aTextureCoord;\n"
        + "}                         \n";

    private final String fragmentShaderCode = "precision mediump float;  \n" + "varying vec2 vTextureCoord;\n"
        + "uniform sampler2D sTexture;\n" + "uniform float fAlpha ;\n" + "void main(){              \n"
        + " vec4 color = texture2D(sTexture, vTextureCoord); \n" + " gl_FragColor = vec4(color.xyz, color.w * fAlpha );\n"
        + " //gl_FragColor = vec4(0.6, 0.7, 0.2, 1.0); \n" + "}                         \n";

    private long startTime;

    private long endTime;
    private DroneControlService droneControlService;

    // ***********************

    public VideoStageRenderer(ControlDroneActivity context, Bitmap initialTexture, boolean isEmulated) {
        this.context = context;
        bgSprite = new GLBGVideoSprite(context.getResources());
        bgSprite.setAlpha(1.0f);

        idSpriteMap = new Hashtable<Integer, Sprite>();
        sprites = new ArrayList<Sprite>(4);
        this.isEmulated = isEmulated;
        // Magic numbers same as in GLBGVideoSprite
        NyARIntSize nyarIS = new NyARIntSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        assetMng = context.getResources().getAssets();
        INyARCameraDistortionFactor distortionFactor = new NyARCameraDistortionFactorV2();
        NyARPerspectiveProjectionMatrix projectionMatrix = new NyARPerspectiveProjectionMatrix();
        // NyARIntSize screenSize = new NyARIntSize(640, 360);
        // this.ar_param=new NyARParam(screenSize, projectionMatrix,
        // distortionFactor);

        try {
            nyARMarkerSystemConfig = new NyARMarkerSystemConfig(SCREEN_WIDTH, SCREEN_HEIGHT);
            // ar_param =
            // NyARParam.createFromARParamFile(assetMng.open("AR/data/hiro.pat"));
            // INyARMarkerSystemConfig inyaSC=new
            // NyARMarkerSystemConfig(this.ar_param);
            nyarM = new NyARMarkerSystem(nyARMarkerSystemConfig);
            this._mid = nyarM.addARMarker(assetMng.open("AR/data/hiro.pat"), 16, 25, 80);

            nyarS = new NyARSensor(nyarIS);

        } catch (NyARException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // this._camera_preview=new CameraPreview(this);
        // this._cap_size=this._camera_preview.getRecommendPreviewSize(320,240);
        // 画面サイズの計算
        // int Sh = this.getWindowManager().getDefaultDisplay().getHeight();
        int screen_w, screen_h;
        // screen_w=(this._cap_size.width*h/this._cap_size.height);
        // screen_h=h;
        // camera
        // fr.addView(this._camera_preview, 0, new
        // LayoutParams(screen_w,screen_h));
        // GLview
        // this._glv=new AndGLView(this);
        // fr.addView(this._glv, 0,new LayoutParams (screen_w,screen_h));

        if (isEmulated) {
            // camera=Camera.open();

            // final Intent cameraIntent = new
            // Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            // int onDrawFrameCAMERA_REQUEST_CODE = 2222;
            // ()context.startActivityForResult(cameraIntent,
            // CAMERA_REQUEST_CODE);
        }
    }

    private void generateTextOnTexture(String textToRender, int[] textures, GL10 gl) {
        // Create an empty, mutable bitmap
        Bitmap bitmap = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_4444);
        // get a canvas to paint over the bitmap
        Canvas canvas = new Canvas(bitmap);
        bitmap.eraseColor(0);

        // get a background image from resources
        // note the image format must match the bitmap format
        Drawable background = context.getResources().getDrawable(R.drawable.barre_bas);
        background.setBounds(0, 0, 256, 256);
        background.draw(canvas); // draw the background to our bitmap

        // Draw the text
        Paint textPaint = new Paint();
        textPaint.setTextSize(32);
        textPaint.setAntiAlias(true);
        textPaint.setARGB(0xff, 0x00, 0x00, 0x00);
        // draw the text centered
        canvas.drawText("Hello World", 16, 112, textPaint);

        // Generate one texture pointer...
        gl.glGenTextures(1, textures, 0);
        // ...and bind it to our array
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

        // Create Nearest Filtered Texture
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        // Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);

        // Use the Android GLUtils to specify a two-dimensional texture image
        // from our bitmap
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

        // Clean up
        bitmap.recycle();
    }

    public void addSprite(Integer id, Sprite sprite) {
        if (!idSpriteMap.containsKey(id)) {
            idSpriteMap.put(id, sprite);
            synchronized (sprites) {
                sprites.add(sprite);
            }
        }
    }

    public Sprite getSprite(Integer id) {
        return idSpriteMap.get(id);
    }

    public void removeSprite(Integer id) {
        if (idSpriteMap.containsKey(id)) {
            Sprite sprite = idSpriteMap.get(id);
            synchronized (sprites) {
                sprites.remove(sprite);
                idSpriteMap.remove(id);
            }
        }
    }

    @SuppressLint("WrongCall")
    public void onDrawFrame(Canvas canvas) {

        frame++;

        bgSprite.onDraw(canvas, 0, 0);

        synchronized (sprites) {
            int spritesSize = sprites.size();

            for (int i = 0; i < spritesSize; ++i) {
                Sprite sprite = sprites.get(i);

                if (!sprite.isInitialized() && screenWidth != 0 && screenHeight != 0) {
                    onSurfaceChanged(canvas, screenWidth, screenHeight);
                    sprite.surfaceChanged(canvas);
                }

                if (sprite != null) {
                    sprite.draw(canvas);
                }
            }
        }

    }

    private StalkerVectorTranslator translator;
    private float lastX = 0, currentX = 0, currentZ = 0;
    private Button btnStalkerOn;
    private Button btnStalkerOff;
    private boolean stalkerOn;

    @SuppressLint("WrongCall")
    public void onDrawFrame(GL10 gl) {

        // Limiting framerate in order to save some CPU time
        endTime = System.currentTimeMillis();
        long dt = endTime - startTime;

        if (dt < 33)
            try {
                Thread.sleep(33 - dt);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        startTime = System.currentTimeMillis();

        // Drawing scene
        bgSprite.onDraw(gl, 0, 0);

        synchronized (sprites) {
            int spritesSize = sprites.size();

            for (int i = 0; i < spritesSize; ++i) {
                Sprite sprite = sprites.get(i);
                if (sprite != null) {
                    if (!sprite.isInitialized() && screenWidth != 0 && screenHeight != 0) {
                        sprite.init(gl, program);
                        sprite.surfaceChanged(null, screenWidth, screenHeight);
                        sprite.setViewAndProjectionMatrices(mVMatrix, mProjMatrix);
                    }

                    sprite.draw(gl);
                }
            }
        }

        // 背景
        // glnya.drawBackGround(cap_image, 1.0);

        if (stalkerOn) {
            executeStalker();
        }
    }

    private void executeStalker() {
        try {
            bgSprite.updateVideoFrame();
            if (phoneDroneLocationListener != null && droneControlService != null) {
                // phoneDroneLocationListener.updateNavGpsData(getDroneControlService().getPrevNavData());
            }
            // generateTextOnTexture("dupa",bgSprite.getTextures(),gl);
            // System.out.println(bytesFromBitmap);
            // fpsN.draw(0, 0);

            synchronized (this.nyarS) {
                frame++;
                short[] bytesFromBitmap = getBytesFromBitmap(bgSprite.getBitmap());
                NyARRgbRaster _rgb_raster =
                    new NyARRgbRaster(bgSprite.getBitmap().getWidth(), bgSprite.getBitmap().getHeight(),
                        NyARBufferType.WORD1D_R5G6B5_16LE);
                _rgb_raster.wrapBuffer(bytesFromBitmap);
                nyarM.setConfidenceThreshold(0.4d);
                nyarM.setBinThreshold(90); 
                nyarS.update(_rgb_raster);
                nyarM.update(nyarS);
                txtVector.setVisible(true);
                String msg = ""+this.nyarM.isExistMarker(this._mid)+" "+String.format("c=%.2f th=%d %d", nyarM.getConfidence(_mid), nyarM.getCurrentThreshold(),nyarM.getLostCount(_mid));
                if (this.nyarM.isExistMarker(this._mid)) {
                    context.stopEmergencySound();
                    if (isFlying) {
                        markerFound = true;
                    }
                    frameLost = 0;
                    getDroneControlService().setJoyEnabled(false);
                    NyARDoubleMatrix44 markerMatrix = nyarM.getMarkerMatrix(this._mid);
                    AdjustmentOrder aorder = null;
                    Log.i("orders", "frame " + frame);

                    if (getDroneControlService() == null) {
                        Log.e("orders", "!!!!!!!!droneControlService is null");
                        return;
                    }
                    if (translator == null) {
                        translator = TranslatorFactory.getInstance().getTranslator(TranslatorStrategy.SIMPLE);
                        translator.init(markerMatrix, nyarM.getFrustum(), SCREEN_WIDTH, SCREEN_HEIGHT);
                        Log.i("orders", "init?" + markerMatrix);
                    } else {
                        setProgressiveCommandEnabled();
                        aorder = translator.translate(markerMatrix);
                        Log.i("orders", "aorder?" + aorder);
                        if (Math.abs(currentX) > 0.001) {
                            lastX = currentX;
                        }
                        if (aorder.getDirection() == AdjustmentOrder.DirectionAdjustmentOrder.GO_LEFT) {
                            currentX = -defaultTurnFactor;
                            // Thread.sleep(100);
                        } else if (aorder.getDirection() == AdjustmentOrder.DirectionAdjustmentOrder.GO_RIGHT) {
                            currentX = defaultTurnFactor;
                        } else {
                            currentX = 0F;
                            Log.i("orders", "STEADY!Q!!");
                        }

                        currentZ = aorder.getSpeed().getSpeedFactor();

                        getDroneControlService().setYawStalker(currentX);
                        getDroneControlService().setPitchStalker(currentZ);
//                        droneControlService.setProgressiveCommandEnabled(false);
                        Log.i("orders", "else?" + aorder);
                    }

                    msg += "FOUND! " + frame + " " + (aorder != null ? aorder.toString() : "");
//                    msg +=
//                        String.format(" X=%.2f Y=%.2f Z=%.2f alt=%d", markerMatrix.m03, markerMatrix.m13, markerMatrix.m23,
//                            getDroneControlService().getPrevNavData().altitude);
                } else {
                    frameLost++;
                    int altitude = getDroneControlService().getPrevNavData().altitude;
                    // txtVector.setText("Altitude: "+altitude);
                    msg += "TO oposite " + frame + "=" + currentX + " " + lastX + " alt= " + altitude;

                    if (!getDroneControlService().isJoyEnabled()) {
                        getDroneControlService().setPitchStalker(0);
//                        getDroneControlService().setProgressiveCommandCombinedYawEnabled(true);
                        getDroneControlService().setProgressiveCommandEnabled(false);

                        if (frameLost > 30) {
                            getDroneControlService().setYawStalker(lastX * 1.5F);
                            context.playEmergencySound();
                        }
                        if (markerFound && frameLost > 245) {
                            if (isFlying) {
                                context.stopEmergencySound();
                                isFlying = false;
                                getDroneControlService().triggerTakeOff();
                            }
                        }
                    }
                }
                Log.i("orders", msg);
                txtVector.setText(msg);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Log.e("ERROR", sw.toString());
            txtVector.setText("Demo mode? " + e.getMessage());
        }
    }

    private void setProgressiveCommandEnabled() {
        droneControlService.setProgressiveCommandEnabled(true);
        droneControlService.setProgressiveCommandCombinedYawEnabled(true);

    }

    private void saveBitmap(int frame, Bitmap bitmap) {
        String path = Environment.getExternalStorageDirectory().toString() + "/dron";
        OutputStream fOut = null;
        File file = new File(path, "Frame" + 1 + ".jpg");
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
        try {
            fOut.flush();
            fOut.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        screenWidth = width;
        screenHeight = height;

        GLES20.glViewport(0, 0, width, height);
        Matrix.orthoM(mProjMatrix, 0, 0, width, 0, height, 0, 2f);

        bgSprite.setViewAndProjectionMatrices(mVMatrix, mProjMatrix);
        bgSprite.onSurfaceChanged(gl, width, height);

        synchronized (sprites) {
            int size = sprites.size();
            for (int i = 0; i < size; ++i) {
                Sprite sprite = sprites.get(i);

                if (sprite != null) {
                    sprite.setViewAndProjectionMatrices(mVMatrix, mProjMatrix);
                    sprite.surfaceChanged(null, width, height);
                }
            }
        }
    }

    public void onSurfaceChanged(Canvas canvas, int width, int height) {
        screenWidth = width;
        screenHeight = height;

        bgSprite.onSurfaceChanged(null, width, height);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        try {

            gl.glMatrixMode(GL10.GL_PROJECTION);
            // gl.glLoadMatrixf(this.nyarM.getGlProjectionMatrix(),0);
            this.text = new AndGLTextLabel(this._glv);
            this.box = new AndGLBox(this._glv, 40);
            this._debug = new AndGLDebugDump(this._glv);
            this.fpsN = new AndGLFpsLabel(this._glv, "MarkerPlaneActivity");
            this.fpsN.prefix = this._cap_size.width + "x" + this._cap_size.height + ":";
        } catch (Exception e) {
            // TODO Auto-generated catch AssetManagerblock
            e.printStackTrace();
            // this.finish();
        }

        startTime = System.currentTimeMillis();

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        program = GLES20.glCreateProgram();
        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);

        GLES20.glLinkProgram(program);
        bgSprite.init(gl, program);

        // Init sprites
        synchronized (sprites) {
            for (int i = 0; i < sprites.size(); ++i) {
                sprites.get(i).init(gl, program);
            }
        }

        Matrix.setLookAtM(mVMatrix, 0, /* x */0, /* y */0, /* z */1.5f, 0f, 0f, -5f, 0, 1f, 0.0f);
    }

    public float getFPS() {
        return fps;
    }

    public boolean updateVideoFrame() {
        return bgSprite.updateVideoFrame();
    }

    private int loadShader(int type, String code) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, code);
        GLES20.glCompileShader(shader);

        int[] compiled = new int[1];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e("opengl", "Could not compile shader");
            Log.e("opengl", GLES20.glGetShaderInfoLog(shader));
            Log.e("opengl", code);
        }

        return shader;
    }

    public void clearSprites() {
        synchronized (sprites) {
            for (int i = 0; i < sprites.size(); ++i) {
                Sprite sprite = sprites.get(i);
                sprite.freeResources();
            }

            sprites.clear();
        }
    }

    private short[] getBytesFromBitmap(Bitmap b) {
        // b is the Bitmap

        // calculate how many bytes our image consists of.
        int bytes = b.getByteCount();
        // int bytes = b.getWidth()*b.getHeight()*4;
        // or we can calculate bytes this way. Use a different value than 4 if
        // you don't use 32bit images.
        // int bytes = b.getWidth()*b.getHeight()*4;

        ByteBuffer buffer = ByteBuffer.allocate(bytes); // Create a new buffer
        b.copyPixelsToBuffer(buffer); // Move the byte data to the buffer

        byte[] byteArray = buffer.array(); // Get the underlying array
                                           // containing the data.

        short[] shortsArray = new short[bytes / 2];
        ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shortsArray);

        return shortsArray;

    }

    public void setTxtVector(Text txtVector) {
        this.txtVector = txtVector;
    }

    public DroneControlService getDroneControlService() {
        return droneControlService;
    }

    public void reset(DroneControlService droneControlService) {
        if (droneControlService != null) {
            this.droneControlService = droneControlService;
            droneControlService.setProgressiveCommandEnabled(false);
        }
        currentX = 0f;
        lastX = 0f;
        translator = null;
        markerFound = false;
        btnStalkerOff.setVisible(false);
        btnStalkerOn.setVisible(true);
        stalkerOn = false;
        context.stopEmergencySound();
    }

    public void enableStalker() {
        btnStalkerOff.setVisible(true);
        btnStalkerOn.setVisible(false);
        stalkerOn = true;

    }

    public void setIsFlying(boolean isFlying) {
        this.isFlying = isFlying;

    }

    public void setBtnStalkerOn(Button btnStalkerOn) {
        this.btnStalkerOn = btnStalkerOn;

    }

    public void setBtnStalkerOff(Button btnStalkerOff) {
        this.btnStalkerOff = btnStalkerOff;
    }

}
