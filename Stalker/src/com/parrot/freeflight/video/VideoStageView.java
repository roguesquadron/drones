package com.parrot.freeflight.video;

import com.tomtom.rogues.stalker.video.SurfaceViewWIthRenderer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class VideoStageView extends SurfaceViewWIthRenderer
implements 
	SurfaceHolder.Callback
{

    
	
	public VideoStageView(Context context) {
		super(context);	
	
		getHolder().addCallback(this);
		
		 fpsPaint.setTextSize(30);
		 fpsPaint.setColor(Color.RED);
	}

	
	
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) 
	{
		super.onSizeChanged(w, h, oldw, oldh);
		
		width = w;
		height = h;
		
		if (renderer != null) {
			renderer.onSurfaceChanged((Canvas)null, w, h);
		}
	}
	




	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) 
	{
		renderer.onSurfaceChanged((Canvas)null, getWidth(), getHeight());		
	}

	


	
   

}
