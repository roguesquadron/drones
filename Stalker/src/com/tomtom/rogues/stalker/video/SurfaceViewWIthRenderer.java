package com.tomtom.rogues.stalker.video;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.parrot.freeflight.video.VideoStageRenderer;
import com.parrot.freeflight.video.VideoStageView;


public class SurfaceViewWIthRenderer extends SurfaceView {
    
    public static boolean SHOW_FPS = false;
    public VideoStageRenderer renderer;
    public DrawThread invalidateThread;
    public int width = 0;
    public int height = 0;
    long timeNow;
    long timePrev = 0;
    long timePrevFrame = 0;
    long timeDelta;
    
    
    //Measure frames per second.
    int framesCount=0;
    int framesCountAvg=0;
    long now;   
    long framesTimer=0;
    
   
 
    

    
   public Paint fpsPaint = new Paint();

    public SurfaceViewWIthRenderer(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public void setRenderer(VideoStageRenderer renderer)
    {
        this.renderer = renderer;
        
        if (width != 0 && height != 0) {
            renderer.onSurfaceChanged((Canvas)null, width, height);
        }
    }
    
    class DrawThread extends Thread {
        private SurfaceHolder surfaceHolder;
        private SurfaceViewWIthRenderer view;
        private boolean run = false;

        public DrawThread(SurfaceHolder surfaceHolder, SurfaceViewWIthRenderer gameView) {
            this.surfaceHolder = surfaceHolder;
            this.view = gameView;
        }

        public void setRunning(boolean run) {
            this.run = run;
        }

        public SurfaceHolder getSurfaceHolder() {
            return surfaceHolder;
        }

        @SuppressLint("WrongCall")
        @Override
        public void run() {
            Canvas c;
            while (run) {
                c = null;

                //limit frame rate to max 60fps
                timeNow = System.currentTimeMillis();
                timeDelta = timeNow - timePrevFrame;
                if ( timeDelta < 16) {
                    try {
                        Thread.sleep(16 - timeDelta);
                    }
                    catch(InterruptedException e) {

                    }
                }
                
                timePrevFrame = System.currentTimeMillis();

                try {
                    renderer.updateVideoFrame();  
                    c = surfaceHolder.lockCanvas(null);

                    synchronized (surfaceHolder) {  
                        view.onDraw(c);
                    }                 
                } finally {
                    if (c != null) {
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }
    }
    
    
    public void onStart()
    {
        onStop();
    }
    
    
    public void onStop()
    {
        if (invalidateThread != null) {
        }
    }
    
    @Override
    protected void onDraw(Canvas canvas) 
    {
        if (renderer != null) {
            renderer.onDrawFrame(canvas);
        }
        else 
            super.onDraw(canvas);
        
        if (SHOW_FPS) {
            now=System.currentTimeMillis();
            canvas.drawText(framesCountAvg + " fps", 80, 70, fpsPaint);
            framesCount++;
            if(now-framesTimer>1000) {
                    framesTimer=now;
                    framesCountAvg=framesCount;
                    framesCount=0;
            }
        }
    }
    
    public void surfaceCreated(SurfaceHolder holder) 
    {
       invalidateThread = new DrawThread(getHolder(), this);
       invalidateThread.setRunning(true);
       invalidateThread.start();
    }
    
    public void surfaceDestroyed(SurfaceHolder holder) 
    {
           boolean retry = true;
           invalidateThread.setRunning(false);
            while (retry) {
                try {
                    invalidateThread.join();
                    retry = false;
                } catch (InterruptedException e) {

                }
            }
    }

}
